# PLEAC-Shell: Write a book about Shell like [PLEAC](http://pleac.sourceforge.net/) project #


## Introduction

- Project Homepage: <http://www.tinylab.org/project/pleac-shell/>
- Project Repository: [https://git.gitlab.com/tinylab/pleac-shell.git](https://gitlab.com/tinylab/pleac-shell)

Please check doc/BUILD.md for building and doc/README.md for the project details.

## Errata

If you see anything that is technically wrong or otherwise in need of
correction, please email me at wuzhangjin at gmail dot com to inform me.

### License

The license is under ![](http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png), see [CC BY NC ND 3.0](http://creativecommons.org/licenses/by-nc-nd/3.0/) for more

