# Notes

*   Header > 4 must end with a `#`
*   To input a `#`, input `\#` instead
*   To input a `$`, input `\$` or \`$\` instead.
